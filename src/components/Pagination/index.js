import React from 'react';
import { MdNavigateBefore, MdNavigateNext } from 'react-icons/md';

import { Container } from './styles';

function Pagination() {
  return (
    <Container>
      <button className="paginate left">
        <MdNavigateBefore size={24} color="#fff" />
      </button>
      <div className="counter"><span>1/124</span></div>
      <button className="paginate right">
        <MdNavigateNext size={24} color="#fff" />
      </button>
    </Container>
  );
}

export default Pagination;

import styled from 'styled-components';

export const Container = styled.div`
  width: 1366px;
  display: flex;
  color: #fff;
  justify-content: flex-end;

  button {
    border: 0;
    border-radius:4px;
    background: #e62429;
    padding: 0 5px;

    display: flex;
    justify-content: center;
    align-items: center;
  }

  span {
    font-size: 20px;
    font-weight: bold;
    padding: 0 10px;
  }
`;

import styled from 'styled-components';

export const Container = styled.header`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin: 30px 0;
  background: #e62429;
  
  a {
    color: #fff;
    font-size: 30px;
    font-weight: bold;
  }
`;

import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { Container } from './styles';

function Header() {
  return (
    <Container>
      <Link to="/">
        MARVEL CHARACTERS
      </Link>
    </Container>
  );
}

export default connect()(Header);

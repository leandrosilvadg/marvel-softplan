import React from 'react';

import { MdSearch } from 'react-icons/md';
import { Container, Form, SearchButton } from './styles';

export default function Search() {
  return (
    <Container>
      <Form onSubmit={() => {}}>
          <input type="text" placeholder="Search Character" />
          <SearchButton>
            <MdSearch size={26} color="#fff"/>
          </SearchButton>
      </Form>
    </Container>
  );
}

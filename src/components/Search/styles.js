import styled from 'styled-components';

export const Container = styled.div`
  width: 1366px;
  padding: 0 0px;
  margin: 0px auto;
`;

export const Form = styled.form`
  margin: 50px 0;
  display: flex; 
  flex-direction: row;

  input {
    flex: 1;
    border: 1px solid #eee;
    padding: 10px 15px;
    border-radius: 4px;
    font-size: 16px;
  }
`;

export const SearchButton = styled.button.attrs({
  type: 'button',
})`
  background: #333;
  border: 0;
  padding: 0 15px;
  margin-left: 5px;
  border-radius: 4px;

  display: flex;
  justify-content: center;
  align-items: center;
`;


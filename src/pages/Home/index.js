import React from 'react';

import { Container } from './styles';

import Search from '../../components/Search';
import Pagination from '../../components/Pagination';


export default function Home() {
  return (
    <Container>
      <Search />
      <Pagination />
    </Container>
  );
}

import React from 'react';

import { Container } from './styles';

export default function Character() {
  return (
    <Container>
      <h1>Character</h1>
    </Container>
  );
}
